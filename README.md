# tp-app-2

- Les recherches à faire:

    - Qu'est ce qu'un fichier YAML?
    - Quelle est la différence entre un fichier YAML et JSON ?
    - Exemple de strucuture avec yml ==> https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-started
    - Prendre connaissance du lien https://www.json2yaml.com/, pour faire du test entre le langage Json et Yml
    - Voir vidéo: https://www.youtube.com/watch?v=7gmW6vxgsRQ&t=623s  Tu laisse une pouce Bleu :thumbsup: pour cocoadmin
    - Créer un fichier YAML avec l'approche Gitlab-ci.
        * Les liens utils:
            - https://docs.gitlab.com/ee/ci/ (Aide à mieux comprendre les différents spectes du CI/CD)
            - https://about.gitlab.com/pricing/ (Une vue d'ensemble des forfaits sur Gitlab-ci)

    - Lancer les jobs definis lors du push du code
    - Lancer les jobs definis lorsqu'on ouvre une merge request aussi quand une merge request est mergé vers la branche **main**
